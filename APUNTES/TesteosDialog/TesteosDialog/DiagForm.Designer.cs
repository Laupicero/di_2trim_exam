﻿
namespace TesteosDialog
{
    partial class DialogsForm
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.rtbTexto = new System.Windows.Forms.RichTextBox();
            this.btnOFD = new System.Windows.Forms.Button();
            this.btnSFD = new System.Windows.Forms.Button();
            this.btnFBD = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // rtbTexto
            // 
            this.rtbTexto.Location = new System.Drawing.Point(39, 221);
            this.rtbTexto.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rtbTexto.Name = "rtbTexto";
            this.rtbTexto.Size = new System.Drawing.Size(758, 295);
            this.rtbTexto.TabIndex = 0;
            this.rtbTexto.Text = "";
            this.rtbTexto.TextChanged += new System.EventHandler(this.rtbTexto_TextChanged);
            // 
            // btnOFD
            // 
            this.btnOFD.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOFD.Location = new System.Drawing.Point(39, 46);
            this.btnOFD.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnOFD.Name = "btnOFD";
            this.btnOFD.Size = new System.Drawing.Size(231, 50);
            this.btnOFD.TabIndex = 1;
            this.btnOFD.Text = "OpenFileDialog";
            this.btnOFD.UseVisualStyleBackColor = true;
            this.btnOFD.Click += new System.EventHandler(this.btnOFD_Click);
            // 
            // btnSFD
            // 
            this.btnSFD.Enabled = false;
            this.btnSFD.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSFD.Location = new System.Drawing.Point(303, 46);
            this.btnSFD.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSFD.Name = "btnSFD";
            this.btnSFD.Size = new System.Drawing.Size(231, 50);
            this.btnSFD.TabIndex = 2;
            this.btnSFD.Text = "SaveFileDialog";
            this.btnSFD.UseVisualStyleBackColor = true;
            this.btnSFD.Click += new System.EventHandler(this.btnSFD_Click);
            // 
            // btnFBD
            // 
            this.btnFBD.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFBD.Location = new System.Drawing.Point(566, 46);
            this.btnFBD.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnFBD.Name = "btnFBD";
            this.btnFBD.Size = new System.Drawing.Size(231, 50);
            this.btnFBD.TabIndex = 3;
            this.btnFBD.Text = "FolderBrowserDialog";
            this.btnFBD.UseVisualStyleBackColor = true;
            this.btnFBD.Click += new System.EventHandler(this.btnFBD_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(60, 186);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(147, 19);
            this.label1.TabIndex = 4;
            this.label1.Text = "Texto Seleccionado:";
            // 
            // DialogsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(850, 552);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnFBD);
            this.Controls.Add(this.btnSFD);
            this.Controls.Add(this.btnOFD);
            this.Controls.Add(this.rtbTexto);
            this.Font = new System.Drawing.Font("Corbel", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "DialogsForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "D I A L O G S";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtbTexto;
        private System.Windows.Forms.Button btnOFD;
        private System.Windows.Forms.Button btnSFD;
        private System.Windows.Forms.Button btnFBD;
        private System.Windows.Forms.Label label1;
    }
}

