﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TesteosDialog
{
    public partial class DialogsForm : Form
    {
        public DialogsForm()
        {
            InitializeComponent();
        }


        // Evento que nos muestra el funcionamiento de OpenFileDialog
        private void btnOFD_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "Abrir archivo";
            //Filtro para ficheros 'txt'
            ofd.Filter = "Ficheros texto|*.txt";
            //ofd.ShowDialog();

            // Para abrir y mostrar en el richTextBox
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                string rutaFichero = ofd.FileName;
                StreamReader reader = new StreamReader(rutaFichero);
                rtbTexto.Text = reader.ReadToEnd();
                reader.Close();
            }
            
        }


        // Evento que nos muestra el funcionamiento de SaveFileDialog
        private void btnSFD_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Title = "Guardar archivo";
            sfd.Filter = "Ficheros texto|*.txt";
            //sfd.ShowDialog();

            //Para guardar lo del richTextBox
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                string rutaFichero = sfd.FileName;
                StreamWriter writer = new StreamWriter(rutaFichero);
                writer.Write(rtbTexto.Text);
                writer.Close();
            }
        }


        // Evento que nos muestra el funcionamiento de FolderBrowser
        // Sólo sirve para seleccionar carpetas
        private void btnFBD_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            // fbd.RootFolder = Environment.SpecialFolder.Personal; - Cambiar el directorio
            // fbd.ShowDialog();

            if (fbd.ShowDialog() == DialogResult.OK)
            {
                String rutaDir = fbd.SelectedPath;
                DirectoryInfo dInfo = new DirectoryInfo(rutaDir);

                foreach(FileInfo fichero in dInfo.GetFiles())
                {
                    rtbTexto.Text += fichero.Name + " " + fichero.DirectoryName + "\n";
                }
            }
        }


        // Por si escribimos o cargamos algún contenido 'txt', nos permita guardarlo
        private void rtbTexto_TextChanged(object sender, EventArgs e)
        {
            if (rtbTexto.TextLength > 0)
                btnSFD.Enabled = true;
            else
                btnSFD.Enabled = false;
        }
    }
}
