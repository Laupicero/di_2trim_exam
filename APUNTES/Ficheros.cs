// FICHEROS DE TEXTO


//==========================
// CLASE FILE: Clase estática
//==========================

//* MÉTODOS
File.AppendText()  //Agrega texto al final de un archivo existente 
File.Copy()  //Copia un archivo 
File.Create()  //Crea o machaca un archivo  
File.CreateText()  //Crea o abre un archivo para escribir texto  
File.OpenText()  //Abre un archivo 
File.Delete()  //Borra un archivo 
File.Move()  //Mueve un fichero 
File.ReadAllLines()  //Lee todas las líneas del fichero  
File.Exists()  //Comprueba que existe un archivo  
File.ReadAllText()  //Lee el contenido completo del archivo  
File.Replace()  //Reemplaza el contenido de un archivo con el contenido de otro archivo 
File.WriteAllText()  //Crea un nuevo archivo y escribe el contenido en él. Si el archivo ya existe, se sobrescribirá.

//Los métodos
CreateText( )
OpenText( )
// devuelven respectivamente un objeto StreamWriter y StreamReader
//Podemos usarlo o no, pero debemos usarlo así, ya que el método File.Create abre un fileStream en el archivo
// Y esto hace que nos salté la excepción de que no se puede usar otro stream o que el fichero está
//siendo usado por otro proceso
File.Create(ruta).Close();

//---------------------------------------------------------------------
//Comprobar si existe o no
 if (File.Exists(nombreFicheroVolcado))  {}
//---------------------------------------------------------------------


//---------------------------------------------------------------------
 //Comprobamos si existe, sino lo creamos
if (File.Exists(nombreFichero))
	ficheroVolcar = File.AppendText(nombreFichero);
else
	ficheroVolcar = File.CreateText(nombreFichero);
//---------------------------------------------------------------------


//---------------------------------------------------------------------
 //Crear Fichero
 StreamWriter ficheroEsc = File.CreateText(nombreFichero);
//---------------------------------------------------------------------


//---------------------------------------------------------------------
 // CrearFichero - Añadir contenido
  StreamWriter ficheroEsc = File.CreateText(nombreFichero);
  ficheroEsc.WriteLine(frase);
  ficheroEsc.Close();
  //---------------------------------------------------------------------


//---------------------------------------------------------------------
// Leer contenido
// También vale así -> StreamReader ficheroLeer = File.OpenText(nombreFichero);
StreamReader reader = new StreamReader(nombreFichero);

string linea = reader.ReadLine();
//Seguimos leyendo hasta que no encuentre ninguna línea
 while (linea != null)
 {
 	linea = reader.ReadLine();
 }
 reader.Close();
 //---------------------------------------------------------------------
//De la  Clase  StreamReader destacamos: 
reader.ReadLine():	lee el archivo línea a línea. 
reader.ReadToEnd():  lee el contenido completo del archivo. 
reader.EndOfStream():  indica si la actual posición está al final del archivo 
reader.Close(): cierra el archivo una vez no necesitemos leerlo más. 
reader.EndOfStream(): Nos devuelve si estamos al final del fichero (en la ultima línea) 


//==========================
// CLASE FILEINFO: Se puede instanciar
//==========================
FileInfo fi = new FileInfo(path);


// Se puede leer
StreamReader reader = fi.OpenText();

string linea = reader.ReadLine();

while(linea != null){
	Console.WriteLine(linea);
	linea = reader.ReadLine();
}


//Borrar
fi.Delete();

//Copiar
fi.CopyTo(path2);

FileInfo mifichero = new FileInfo(path);
if (mifichero.Exists)
{
  Console.WriteLine("Nombre: {0}", mifichero.Name);
  Console.WriteLine("Nombre completo: {0}", mifichero.FullName);
  Console.WriteLine("Directorio padre: {0}", mifichero.DirectoryName);
  Console.WriteLine("Es de lectura: {0}", mifichero.IsReadOnly);
  Console.WriteLine("Tamaño: {0} bytes", mifichero.Length);
  Console.WriteLine("Atributos: {0}", mifichero.Attributes);
  Console.WriteLine("Creado: {0}", mifichero.CreationTime);
  Console.WriteLine("Leído: {0}", mifichero.LastAccessTime);
  Console.WriteLine("Modificado: {0}", mifichero.LastWriteTime);
}


 //***************************************************
 // 					EJERCICIOS
 //***************************************************
di_colegio_2020  //MDIContainer