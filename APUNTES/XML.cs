// XML

// -------------------------
// Leer Datos de un XML: Disponemos de 2 Clases
// -------------------------
xmlReader //-> Más Rápido/Eficiente. Con esta clase, siempre se lee hacia adelante y nunca hacia atrás, 
xmlDocument //-> Más Flexible

//=============================================
// EJEMPLO xmlReader
//=============================================


//-----------------
// Leer uno por uno los datos de los empleados
// Y obtenemos los nodos que nos interesan
XmlReader reader = XmlReader.Create(@"empleados.xml");

while (reader.Read()){
	//Rellenamos el Combobox de los 'ID'
    if (this.reader.Name.ToString() == "idEmpleado")
        cbIDEmpleado.Items.Add(this.reader.ReadString());

    //Rellenamos el Combobox del 'nombre' y los 'apellidos'
    if (this.reader.Name.ToString() == "nombre")
        nombreCompletoEmpleado += this.reader.ReadString() + " ";

    if (this.reader.Name.ToString() == "apellidos")
    {
        nombreCompletoEmpleado += this.reader.ReadString();
        cbNombreEmpleado.Items.Add(nombreCompletoEmpleado);
        nombreCompletoEmpleado = "";
    }

}


//-----------------
// Nos devolverá una Lista de 'string' con todos los resultados del nodo que haya encontrado
List<string> result = new List<string>();
string rutaFicheroXML = directorioActual + "\\" + nombreFichero;

XmlReader reader = XmlReader.Create(rutaFicheroXML);

try
{
    while (reader.Read())
    {
        if (reader.IsStartElement() && reader.Name.ToString() == nombreNodo)
            result.Add(reader.ReadElementContentAsString());
    }
    reader.Close();
    return result;
} catch(Exception ex)
{
    reader.Close();
    return result;
}


//-----------------
// Nos devolverá un 'string' con el resultado del nodo que haya encontrado
string result = "";
string rutaFicheroXML = directorioActual + "\\" + nombreFichero;

XmlReader reader = XmlReader.Create(rutaFicheroXML);
try
{
    while (reader.Read())
    {
        if (reader.IsStartElement() && reader.Name.ToString() == nombreNodo)
            result = reader.ReadElementContentAsString();
    }
    reader.Close();
    return result;
} catch(Exception ex)
{
    reader.Close();
    return result;
}

//=============================================
// EJEMPLO xmlDocument
//=============================================
//Mostramos los datos del empleado seleccionado a partir de su 'ID'
XmlDocument doc = new XmlDocument();
doc.Load("empleados.xml"); 
String strExpres = "/empleados/empleado[idEmpleado='" + cbIDEmpleado.SelectedItem.ToString() + "']";

foreach (XmlNode node in nodeList){
	XmlElement element = (XmlElement)node;

    String datosEmpleado += element.GetElementsByTagName("idEmpleado")[0].Name + " " + element.GetElementsByTagName("idEmpleado")[0].InnerText + "\r\n" 
        + element.GetElementsByTagName("nombre")[0].Name + " " + element.GetElementsByTagName("nombre")[0].InnerText + "\r\n" +
        element.GetElementsByTagName("apellidos")[0].Name + " " +  element.GetElementsByTagName("apellidos")[0].InnerText + "\r\n"

        + element.GetElementsByTagName("telefonos")[0].ChildNodes[0].Name + " " +  element.GetElementsByTagName("telefonos")[0].ChildNodes[0].InnerText + "\r\n" +
        element.GetElementsByTagName("telefonos")[0].ChildNodes[1].Name + " " +  element.GetElementsByTagName("telefonos")[0].ChildNodes[1].InnerText;
}


//-----------------
//Mostramos los datos del empleado seleccionado a partir de su 'nombre' y 'apellidos'
XmlDocument doc = new XmlDocument();
doc.Load("empleados.xml");
String strExpres = "/empleados/empleado[nombre='" + nombreEmpleado + "' and apellidos='" + apellidosEmpleado + "']";

XmlNodeList nodeList = doc.SelectNodes(strExpres);

//Para comprobar si la expresión es correcta
//int count1 = nodeList.Count;
foreach (XmlNode node in nodeList)
{
    XmlElement element = (XmlElement)node;
    String datos += element.GetElementsByTagName("idEmpleado")[0].Name + " " + element.GetElementsByTagName("idEmpleado")[0].InnerText + "\r\n"
        + element.GetElementsByTagName("nombre")[0].Name + " " + element.GetElementsByTagName("nombre")[0].InnerText + "\r\n" +
        element.GetElementsByTagName("apellidos")[0].Name + " " + element.GetElementsByTagName("apellidos")[0].InnerText + "\r\n"
        + element.GetElementsByTagName("telefonos")[0].ChildNodes[0].Name + " " + element.GetElementsByTagName("telefonos")[0].ChildNodes[0].InnerText + "\r\n" +
        element.GetElementsByTagName("telefonos")[0].ChildNodes[1].Name + " " + element.GetElementsByTagName("telefonos")[0].ChildNodes[1].InnerText;
}


//-----------------
//Obtener un libro a partit de su titulo
XmlDocument docxml = new XmlDocument();
docxml.Load("libros.xml");

XmlNode nodo = this.docxml.SelectSingleNode("//biblioteca/libro[titulo='" + listBoxTituloLibros.SelectedItem.ToString() + "']");

if (nodo != null)
{
    tbGenero.Text = nodo.Attributes["genero"].InnerText;
    tbFechaPublicacion.Text = nodo.Attributes["fechadepublicacion"].InnerText;
    tbISBN.Text = nodo.Attributes["ISBN"].InnerText;
    tbAutor.Text = nodo.SelectSingleNode("autor/nombre").InnerText;
    tbApellidosAutor.Text = nodo.SelectSingleNode("autor/apellido").InnerText;
    tbPrecio.Text = nodo.SelectSingleNode("precio").InnerText;
}


//-----------------
//Nos rellena el listBox con los titulos de los libros al leer desde su 'xml'
XmlDocument docxml = new XmlDocument();
docxml.Load("libros.xml");

XmlNodeList libros = this.docxml.SelectNodes("//biblioteca/libro/titulo");

foreach (XmlElement item in libros)
    istBoxTituloLibros.Items.Add(item.InnerText);



//-----------------
// CRUD
//-----------------

private XmlDocument docxml = new XmlDocument();
this.docxml.Load("libros.xml");

//-----------------
//INSERTAR
XmlNode nodoraiz = this.docxml.DocumentElement;
XmlNode nodolibro = docxml.CreateElement("libro");

 //Creamos los atributos y los nodos
XmlAttribute xmlAtrib1 = this.docxml.CreateAttribute("genero");
XmlAttribute xmlAtrib2 = this.docxml.CreateAttribute("fechadepublicacion");
XmlAttribute xmlAtrib3 = this.docxml.CreateAttribute("ISBN");
XmlElement nodoTitulo = docxml.CreateElement("titulo");
XmlElement nodoAutor = this.docxml.CreateElement("autor");

 // Añadimos los atributos y los nodos
nodolibro.Attributes.Append(xmlAtrib1);
nodolibro.Attributes.Append(xmlAtrib2);
nodolibro.Attributes.Append(xmlAtrib3);
nodolibro.AppendChild(nodoTitulo);
nodoAutor.AppendChild(nodoAutorNombre);

// Le damos valor
nodolibro.Attributes["genero"].InnerXml = tbGenero.Text;
nodolibro.Attributes["fechadepublicacion"].InnerXml = tbFechaPublicacion.Text;
nodolibro.Attributes["ISBN"].InnerXml = tbISBN.Text;
nodolibro.SelectSingleNode("titulo").InnerText = tbTitulo.Text;
nodolibro.SelectSingleNode("autor/nombre").InnerText = tbAutor.Text;

// Añadimos el nuevoelemento
nodoraiz.AppendChild(nodolibro);
this.docxml.Save("libros.xml");


//-----------------
//ACTUALIZAR
XmlNode nodo = this.docxml.SelectSingleNode("//biblioteca/libro[titulo='" + listBoxTituloLibros.SelectedItem.ToString() + "']");
                
nodo.Attributes["genero"].InnerText = tbGenero.Text;
nodo.Attributes["fechadepublicacion"].InnerText = tbFechaPublicacion.Text;
nodo.Attributes["ISBN"].InnerText = tbISBN.Text;
nodo.SelectSingleNode("titulo").InnerText = tbTitulo.Text;
nodo.SelectSingleNode("autor/nombre").InnerText = tbAutor.Text;
nodo.SelectSingleNode("autor/apellido").InnerText = tbApellidosAutor.Text;
nodo.SelectSingleNode("precio").InnerText = tbPrecio.Text;
this.docxml.Save("libros.xml");

actualizarDatosListBox(listBoxTituloLibros.SelectedItem.ToString(), tbTitulo.Text);


//-----------------
// BORRAR
 XmlNode nodoraiz = this.docxml.DocumentElement;

if (listBoxTituloLibros.SelectedIndex >= 0 && listBoxTituloLibros.SelectedItem != null)
{
    XmlNode nodo = this.docxml.SelectSingleNode("//biblioteca/libro[titulo='" + listBoxTituloLibros.SelectedItem.ToString() + "']");                
    nodoraiz.RemoveChild(nodo);
    his.docxml.Save("libros.xml");

    borrardelListBox(listBoxTituloLibros.SelectedItem.ToString());
    borrarTextBox();
}