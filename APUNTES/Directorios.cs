// DIRECTORIOS
//Si queremos analizar el contenido de un directorio, podemos emplear la clase Directory

//====================================
// 		CLASE DIRECTORY: Clase estática
//====================================

//------------------------------------------------------------------
//Directory.GetCurrentDirectory
//Obtiene el directorio de trabajo actual de la aplicación.
string path = Directory.GetCurrentDirectory();

// Si queremos saber si existe
Directory.Exists(path);

// Crear Directorio sino existe
if(!Directory.Exists(path))
	Directory.CreateDirectory(path);
//------------------------------------------------------------------




//-----------------------------------------------------------------
	// MÉTODOS ÚTILES

Directory.Delete() //borra un directorio 
Directory.Move() //Mueve un directorio 
Directory.Exists () //Existe el directorio 
Directory.GetFiles() //obtiene la lista de ficheros que contiene un directorio
//------------------------------------------------------------------

//------------------------------------------------------------------
// En el ejemplo siguiente se muestra cómo recuperar todos los archivos de texto de un directorio 
// y moverlos a un nuevo directorio.
string sourceDirectory = @"C:\current";
string archiveDirectory = @"C:\archive";

try
{
	string[] txtFiles = Directory.EnumerateFiles(sourceDirectory, "*.txt");

	foreach (string currentFile in txtFiles)
    {
        string fileName = currentFile.Substring(sourceDirectory.Length + 1);
        Directory.Move(currentFile, Path.Combine(archiveDirectory, fileName));
    }
}
catch (Exception e)
{
    Console.WriteLine(e.Message);
}
//------------------------------------------------------------------


//====================================
// 		CLASE DIRECTORYINFO: Se puede heredar de ésta
//====================================
//Expone métodos de instancia para crear, mover y enumerar archivos en directorios y subdirectorios. Esta clase no puede heredarse.

var dirInfo = new DirectoryInfo("C:\\Temp");

if (dirInfo.Exists) {
	//Recorremos los ficheros
    FileInfo[] files = dirInfo.GetFiles("*.*", SearchOption.AllDirectories);

    foreach (FileInfo file in files){
        Console.WriteLine(file.Name);
   	}

    //Recorremos los directorios
    DirectoryInfo[] diArr = dirInfo.GetDirectories()

    foreach (DirectoryInfo dri in diArr){
        Console.WriteLine(dri.Name);
   	}    
}

// Atributos
dirInfo.CreationTime
dirInfo.CreationTimeUtc
dirInfo.LastAccessTime
dirInfo.LastAccessTimeUtc
dirInfo.LastWriteTime
dirInfo.LastWriteTimeUtc