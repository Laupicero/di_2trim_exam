// DATABASES
//---------------------------------------------------------------------

//---------------------
//REALIZAR CONEXIÓN NORMAL

try
{
	string cadenaDeConexion = @"Server=DESKTOP-A795OQI;Database=BDPasaje; Integrated Security=true;";
    SqlConnection conexion = new SqlConnection(cadena);
    conexion.Open();

    //Realizar Login
     SqlCommand command = new SqlCommand("SELECT * FROM Cliente WHERE IIDCLIENTE = " + userID + " AND NOMBRE= '" + userName + "';", conexion);
     SqlDataReader dataReader = command.ExecuteReader();


    while (dataReader.Read())
    {
        Console.WriteLine("\n-> ID: " + dataReader[0] + "\tNombre:" + dataReader["Nombre"] + "\tCorreo:" + dataReader["EMAIL"]);
        contadorResultados++;
    }

    if (contadorResultados == 0)
        Console.WriteLine("No se han encontrado Resultados que coinidan");
                    


    //Liberar Mmoria
    dataReader.Close();
    //Cerrar Conex
    conexion.Close();

}catch (Exception ex)
{
    //Console.WriteLine("Problema al tratar de conectar a BD. Detalles:");
    //Console.WriteLine(ex.Message);
    MessageBox.show();
}
//--------------------------

//---------------------
//REALIZAR CONEXIÓN A TRAVÉS DE APP.CONFIG
<!-- Conexiones -->
	<connectionStrings>
		<add name="conAlvaro" connectionString="SERVER=localhost;DATABASE=saha;UID=root;PASSWORD=abc123;max pool size=50000000; Allow User Variables=True"/>
		<add name="conLaura" connectionString="Server=DESKTOP-GVEQ5O6;Database=OkiSpain; Integrated Security=true;"/>
		<add name="conAlmunia" connectionString="Server=DESKTOP-SGN3LO3;Database=OkiSpain; Integrated Security=true;"/>
</connectionStrings>

//Obtiene el 'string' de la conexión a través del 'app.config'
private SqlConnection conexion;

string cadenaDeConexion =  ConfigurationManager.ConnectionStrings["conLaura"].ConnectionString;
conexion = new SqlConnection(cadenaDeConexion);
conexion.Open();





 //***************************************************
 // 					EJERCICIOS
 //***************************************************

//**********
// CRUD
//**********


//**********
// CREATE - INSERTAR
//**********
try
{
    String query = "INSERT INTO Empleado(NOMBRE, APPATERNO, APMATERNO, FECHACONTRATO, SUELDO, IIDTIPOUSUARIO, " +
    "IIDTIPOCONTRATO, IIDSEXO, BHABILITADO, bTieneUsuario, TIPOUSUARIO)" +

    "VALUES('" + emp.Nombre + "', '" + emp.Apll1 + "', '" + emp.Apll2 + "', '" + emp.FechaContrato + "', " + emp.Sueldo + ", "
    + emp.IdTipoUsuario + ", " + emp.IdTipoContrato + ", " + emp.IdSexo + ", " + emp.BHabilitado + ", " + emp.TieneUsuario + ", '" + emp.TipoUsuario + "')";

    SqlCommand cmd = new SqlCommand(query, this.conexion);
                
    cmd.ExecuteNonQuery();
    MessageBox.Show("¡Nuevo Empleado Insertado con ÉXITO!", "OPERACIÓN REALIZADA");
}
catch (Exception e)
{
    MessageBox.Show("Vaya... Al parecer algo falló: \n" + e.Message, "ERROR");
}


//**********
// READ - SELECT
//**********
//----------------
//Rellenamos los ComboBox a la hora en la que vayamos a insertar un nuevo Empleado
SqlCommand cmd;
SqlDataReader dataReader;

//ComboBox-sexo
cmd = new SqlCommand("Select nombre from Sexo", this.conexion);
dataReader = cmd.ExecuteReader();
while (dataReader.Read())
    cmbSexo.Items.Add(dataReader["nombre"]);
dataReader.Close();

//ComboBox-TipoContrato
cmd = new SqlCommand("Select nombre from tipocontrato", this.conexion);
dataReader = cmd.ExecuteReader();
while (dataReader.Read())
    cmbContrato.Items.Add(dataReader["nombre"]);
dataReader.Close();

//ComboBox-TipoUsuario
            cmd = new SqlCommand("Select nombre from tipousuario", this.conexion);
dataReader = cmd.ExecuteReader();
while (dataReader.Read())
    cmbTipousuario.Items.Add(dataReader["nombre"]);
dataReader.Close();



//----------------
 // Nos devuelve a través de un 'char' el nombre del tipo de usuario
 SqlCommand cmd;
 SqlDataReader dataReader;
 String tipoUsuarioString = "";
 cmd = new SqlCommand("SELECT NOMBRE FROM TIPOUSUARIOREGISTRO WHERE TIPOUSUARIO = '" + tipoUsuario + "'", this.conexion);

 dataReader = cmd.ExecuteReader();

 while (dataReader.Read())
     tipoUsuarioString = dataReader["NOMBRE"].ToString();

 dataReader.Close();

 return tipoUsuarioString;


//**********
// UPDATE - ACTUALIZAR
//**********
try
{
    String query = "UPDATE Empleado SET NOMBRE = '" +emp.Nombre+ "', APPATERNO = '" +emp.Apll1+ "', APMATERNO = '" +emp.Apll2+ "', FECHACONTRATO = '" +emp.FechaContrato.ToString("yyyy-MM-dd HH:mm:ss.fff") + "'," +
        "SUELDO = " +emp.Sueldo+  ",  BHABILITADO = " +emp.BHabilitado+ ", bTieneUsuario = " +emp.TieneUsuario+ ", TIPOUSUARIO = '" +emp.TipoUsuario+ "'" +
        "Where IIDEMPLEADO = " + emp.Id;

    SqlCommand cmd = new SqlCommand(query, this.conexion);
    cmd.ExecuteNonQuery();

    MessageBox.Show("¡Empleado Actualizado con ÉXITO!", "OPERACIÓN REALIZADA");
    controlHabilitarDeshabilitarFormulario(false);                

}
catch (Exception e)
{
    MessageBox.Show("Vaya... Al parecer algo falló: \n" + e.Message, "ERROR");
}


//**********
// DELETE - BORRAR
//**********
try
{
    String query = "Delete From Empleado Where IIDEMPLEADO = " + Convert.ToInt32(lblCodigo.Text);

    SqlCommand cmd = new SqlCommand(query, this.conexion);
    cmd.ExecuteNonQuery();

    MessageBox.Show("¡Empleado BORRADO con ÉXITO!", "OPERACIÓN REALIZADA");

    //Borramos al Empleado de la Lista
    this.lstEmp.RemoveAt(this.empleadoIndex);

    //Mostramos el empleado anterior sino hemos borrado el primero
    if (this.empleadoIndex != 0)
        this.empleadoIndex -= 1;

    rellenarItemsConDatosEmpleado(this.lstEmp[this.empleadoIndex]);

}
catch (Exception ex)
{
    MessageBox.Show("Vaya... Al parecer algo falló: \n" + ex.Message, "ERROR");
}