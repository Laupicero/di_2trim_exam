//--------------------------
// 		DATAGRIDVIEW
//--------------------------

// A través de una DB
/// Nos rellenará nuestro DataGridView de Empleados y lo bindeará
// con el bindingNavigator
SqlDataAdapter sqlAdapter = new SqlDataAdapter("Select * from Empleado", this.conexion);
DataSet dataSet = new DataSet();
sqlAdapter.Fill(dataSet, "Empleado");
DataTable dataTable = dataSet.Tables["Empleado"];

BindingSource bs = new BindingSource();
bs.DataSource = dataSet.Tables[0].DefaultView;

bnEmpleado.BindingSource = bs;
dgvEmpleado.DataSource = bs;

// Rellenar DGV Y Bindear con BindingNav
SqlDataAdapter sqlAdapter = new SqlDataAdapter("Select * from Empleado", this.conexion);
DataSet ds = new DataSet();
sqlAdapter.Fill(ds);

BindingSource bs = new BindingSource();
bs.DataSource = ds.Tables[0].DefaultView;
bnEmpleado.BindingSource = bs;
dgvEmpleado.DataSource = bs;

 // Rellenar Sólo DGV
 String consulta = "Select * from Empleado";
 SqlDataAdapter sda = new SqlDataAdapter(consulta, this.conexion);
 DataTable dt = new DataTable();
 sda.Fill(dt);
 dgvEmpleado.DataSource = dt;




//--------------------------
// 		BINDING NAVIGATOR
//--------------------------
 //Para el CRUD
this.commandBuilder = new SqlCommandBuilder(sqlAdapter);
sqlAdapter.InsertCommand = this.commandBuilder.GetInsertCommand();
sqlAdapter.InsertCommand = this.commandBuilder.GetDeleteCommand();
sqlAdapter.InsertCommand = this.commandBuilder.GetUpdateCommand();

 //***************************************************
 // 					EJERCICIOS
 //***************************************************
di_db_crud_empleadosDATASET