﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConexionEncriptada.Entities
{
    class Usuario
    {
        private String nombreUsuario;
        private String contrasenna;


        //Constructor
        public Usuario(string nombreUsuario, string contrasenna)
        {
            this.nombreUsuario = nombreUsuario;
            this.contrasenna = contrasenna;
        }

        //Modificadores de acceso
        public string NombreUsuario { get => nombreUsuario; set => nombreUsuario = value; }
        public string Contrasenna { get => contrasenna; set => contrasenna = value; }
    }
}
