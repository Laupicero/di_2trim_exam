﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConexionEncriptada
{
    public partial class Form1 : Form
    {
        private Controllers.Cntr_Menu ctrl;
        private List<Entities.Empleado> empleados;


        //Constructor
        public Form1()
        {
            InitializeComponent();
        }


        //Evento LOAD
        private void Form1_Load(object sender, EventArgs e)
        {
            this.ctrl = new Controllers.Cntr_Menu();
            this.empleados = new List<Entities.Empleado>();
        }

        //-----------------------------------
        //EVENTOS BOTONES
        //-----------------------------------

        //Botón generar ficheros de las nóminas de nuestros empleados
        private void btnGenerar_Click(object sender, EventArgs e)
        {
            if (this.ctrl.generarListadoEmpleados(ref empleados))
            {
                //Nos calcula el complemento del cargo por cada empleado
                if(this.ctrl.calcularComplementoCargo(ref empleados))
                {
                    //Nos calcula el complemento de responsabilidad/investg por cada empleado
                    this.ctrl.calcularComplementoResponsabilidad(ref empleados);

                    this.ctrl.calcularSueldoNeto(ref empleados);

                    //Genera los ficheros indep con las nóminas de los empleados
                    if (this.ctrl.generarNominas(empleados))
                    {
                        MessageBox.Show("Se han generado correctamente todas las nóminas", "INFO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                        MessageBox.Show("No se pudieron crear las nóminas de los empleados", "I N F O", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                    MessageBox.Show("No se pudo calcular el complemento de cargo de los empleados", "I N F O", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
                MessageBox.Show("No se pudo crear/obtener el listado de empleados", "I N F O", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        //Botón que enviará el correo de las nóminas a cada uno de los empleados
        private void btnEnviar_Click(object sender, EventArgs e)
        {

        }

        
    }
}
