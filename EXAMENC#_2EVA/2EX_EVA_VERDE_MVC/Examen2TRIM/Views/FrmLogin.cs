﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConexionEncriptada
{
    public partial class frmLogin : Form
    {
        private Controllers.Ctrl_Login ctrl;
        private Utilities.AES aes;

        //Constructor
        public frmLogin()
        {
            InitializeComponent();
        }


        //-------------------------------
        // EVENTOS FORMULARIO
        //-------------------------------

        //---------
        //EVENTO LOAD
        //Nos carga los componentes del formulario y nos realiza la conexión
        private void frmLogin_Load(object sender, EventArgs e)
        {
            this.ctrl = new Controllers.Ctrl_Login();
            this.aes = new Utilities.AES();

            if(!this.ctrl.generarConexion())
                MessageBox.Show("No se ha podido conectar a la DB", "I N F O", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }


        //BOTÓN LOGIN
        //Nos realiza el login de la aplicación para poder acceder al menú
        private void btnlogin_Click(object sender, EventArgs e)
        {
            // Comprobamos previamente que nuestros campos están rellenos
            // if(txtId.TextLength > 0 && txtPwd.TextLength >0) ---> Así también podría valer
            if (this.ctrl.comprobarCamposRellenos(txtId.Text, txtPwd.Text))
            {
                // Nos devuelve la passw encriptada para comprobarla con la de la DB
                string pass = this.aes.Encrypt(txtPwd.Text, Utilities.AES.appPwdUnique, int.Parse("256"));

                if (this.ctrl.comprobarCredenciales(txtId.Text, pass))
                {
                    this.ctrl.abrirVista_VentanaMenu();
                }
                else
                    MessageBox.Show("Usuario no existente o incorrecto", "I N F O", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
                MessageBox.Show("Debe rellenar primero los campos", "I N F O", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }


        //BOTÓN CANCELAR
        //Nos borra el contenido de los 'textBox'
        private void btnCancel_Click(object sender, EventArgs e)
        {
            txtId.Clear();
            txtPwd.Clear();
        }

        

        // EVENTO 'CLOSING'
        // sucede al darle al botón de cerrar la aplicación
        // Nos pregunta si queremos salir o no de ésta
        private void frmLogin_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult confimacion = MessageBox.Show("¿Salir del programa, estás seguro?", "S A L I R", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information);

            if (confimacion == DialogResult.Yes)
                e.Cancel = false;
            else
                e.Cancel = true;
        }

    }
}
