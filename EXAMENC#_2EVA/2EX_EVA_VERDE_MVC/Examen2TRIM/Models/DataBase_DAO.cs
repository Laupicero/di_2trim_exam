﻿using ConexionEncriptada.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConexionEncriptada.Models
{
    class DataBase_DAO
    {
        private SqlConnection conexion;

        //Cosntructor
        public DataBase_DAO(){}


        // Obtenemos un listado con las características básicas de los empleados
        internal bool obtenerEmpleados(ref List<Empleado> empleados)
        {
            SqlCommand cmd = new SqlCommand("Select * FROM empleado", this.conexion);
            SqlDataReader rd = cmd.ExecuteReader();

            while (rd.Read())
            {
                try
                {
                    Empleado e = new Empleado();
                    e.Nombre = rd["nombre"].ToString();
                    e.Apell = rd["apellido"].ToString();
                    e.IdCargo = rd["idcargo"].ToString();
                    e.IdDepart = Convert.ToInt32(rd["iddepartamento"].ToString());
                    e.SueldoBruto = Convert.ToDouble(rd["sueldo"].ToString()) / 12;

                    if (!rd.IsDBNull(9))
                        e.Comision = Convert.ToDouble(rd["comision"].ToString()) / 12;

                    empleados.Add(e);
                }
                catch (Exception ex)
                {
                    rd.Close();
                    return false;
                }
            }
            rd.Close();
            return true;
        }

        // Nos obtiene el complemento del cargo de los empleados que lo tengan
        internal bool obtenerComplementoCargo(ref List<Empleado> empleados)
        {
            foreach (Empleado e in empleados)
            {
                SqlCommand cmd = new SqlCommand("select * from cargo where idcargo = '" + e.IdCargo + "'", this.conexion);
                SqlDataReader rd = cmd.ExecuteReader();

                while (rd.Read())
                {
                    try
                    {
                        e.ComplementoCargo = Convert.ToDouble(rd["sueldo_min"]) / 12;
                    }
                    catch (Exception ex)
                    {
                        rd.Close();
                        return false;
                    }
                }
                rd.Close();
            }
            return true;
        }


        // Nos genera la conexion a nuestra DB
        internal bool generaConexion()
        {
            try
            {
                //Obtiene el 'string' de la conexión a través del 'app.config'
                string cadenaDeConexion = ConfigurationManager.ConnectionStrings["conLaura"].ConnectionString;
                conexion = new SqlConnection(cadenaDeConexion);
                conexion.Open();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        // Nos comprueba si existe un determinado usuario en nuestra DB
        // Sino existe o saltase una excepción, nos devolverá un false
        internal bool comprobarUsuarioEnDB(Usuario usuario)
        {
            bool existe = false;
            SqlCommand cmd = new SqlCommand("Select * FROM Usuario WHERE nombreUsuario = '" + usuario.NombreUsuario + "'" +
                " AND contrasena='" + usuario.Contrasenna + "'", this.conexion);
            SqlDataReader rd = cmd.ExecuteReader();

            while (rd.Read())
            {
                try
                {
                    existe = true;
                }
                catch (Exception e)
                {
                    rd.Close();
                }
            }
            rd.Close();
            return existe;
        }
    }
}
