﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConexionEncriptada.Controllers
{
    /// <summary>
    /// Clase que se encarga de vincular la vista del formulario de Login 
    /// con el modelo de datos correspondiente
    /// </summary>
    class Ctrl_Login
    {
        private Models.DataBase_DAO db;


        //Constructor
        public Ctrl_Login()
        {
            this.db = new Models.DataBase_DAO();
        }


        // Nos comprueba si nuestros campos están rellenos o no
        // Nois devolverá un valor 'bool' con el resultado
        internal bool comprobarCamposRellenos(string user, string pass)
        {
            bool camposRellenos = true;

            if (String.IsNullOrEmpty(user) || String.IsNullOrEmpty(pass))
                camposRellenos = false;

            return camposRellenos;

        }

        // Nos generá nuestra conexión sino está creada
        internal bool generarConexion()
        {
            return this.db.generaConexion();
        }

        // Nos compureba nuestras credenciales con nuestra DH
        // Si nuestro usuario es correcto o no
        internal bool comprobarCredenciales(string usuarioId, string pass)
        {
            bool usuarioEncontrado = false;
            //Comprobamos si exste el usuario
            // Le pasamos a nuestro método una instancia de usuario
            usuarioEncontrado = this.db.comprobarUsuarioEnDB(new Entities.Usuario(usuarioId, pass));
            return usuarioEncontrado;
        }

        // Nos abre la ventana del nuevo formulario
        internal void abrirVista_VentanaMenu()
        {
            Form1 formMenu = new Form1();
            formMenu.ShowDialog();
        }
    }
}
