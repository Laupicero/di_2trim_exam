﻿using ConexionEncriptada.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConexionEncriptada.Controllers
{
    class Cntr_Menu
    {
        private Models.DataBase_DAO db;

        public Cntr_Menu() { }


        // Nos devuelve relleno nuestro listado de empleados y a través de un 'bool'
        // Nos dirá si la operación ha tenido o no éxito
        internal bool generarListadoEmpleados(ref List<Empleado> empleados)
        {
            return this.db.obtenerEmpleados(ref empleados);
        }


        // Nos devuelve nuestro listado de empelados modificado y a través de un 'bool'
        // Nos dirá si la operación ha tenido o no éxito
        internal bool calcularComplementoCargo(ref List<Empleado> empleados)
        {
            return this.db.obtenerComplementoCargo(ref empleados);
        }


        //Nos calcula el complemento de responsabilidad por cada empleado
        internal void calcularComplementoResponsabilidad(ref List<Empleado> empleados)
        {
            foreach (Empleado e in empleados)
            {
                if (e.IdDepart == 100)
                    e.ComplementoResp = 16000 / 12;

                if (e.IdDepart == 102)
                    e.ComplementoResp = 10000 / 12;
            }
        }

        //Nos calcula el sueldo Neto de los empleados aplicando las retenciones
        internal void calcularSueldoNeto(ref List<Empleado> empleados)
        {
            foreach (Empleado e in empleados)
            {

                e.Irpf = e.SueldoBruto * 0.15;
                e.Desempleo = e.SueldoBruto * 0.03;

                e.SueldoNeto = e.SueldoBruto - (e.Irpf + e.Desempleo);
            }
        }

        // Nos genera los ficheros con las nóminas de nuestros empleados
        internal bool generarNominas(List<Empleado> empleados)
        {
            if (generarNominasIndividuales(empleados) && generarResumenNominas(empleados))
                return true;
            else return false;
           
        }


        //Nos genera un resumen con todas las nóminas de los empleados
        // Estos datos irán a parar a otro correo/destinatario para así poder llevar una contabilidad
        private bool generarResumenNominas(List<Empleado> empleados)
        {
            try
            {
                // Nos creamos por cada empleado un fichero
                string ruta = "ficherosNominaEmpleados\\Laura_RESUMEN_NOMINA.txt";
                StreamWriter writer = new StreamWriter(ruta);

                // Recorremos nuestro listado de empleados
                foreach (Empleado e in empleados)
                    writer.WriteLine("\nNÓMINA DE " + e.Nombre + " " + e.Apell + "\tSUELDO NETO: " + e.SueldoNeto);
                writer.Close();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }


        // Nos generá un fichero 'txt' por cada uno de los empleados con su nómina
        private bool generarNominasIndividuales(List<Empleado> empleados)
        {
            try
            {
                // Recorremos nuestro listado de empleados
                foreach (Empleado e in empleados)
                {
                    // Creamos un directorio para guardar las nóminas de nuestros empleados
                    if (!Directory.Exists("ficherosNominaEmpleados"))
                    {
                        Directory.CreateDirectory("ficherosNominaEmpleados");
                    }


                    // Nos creamos por cada empleado un fichero
                    string ruta = "ficherosNominaEmpleados\\Laura_Empleado" + e.Nombre + "_" + e.Apell + ".txt";
                    e.RutaFicheroNomina = ruta;

                    //Podemos usarlo o no, pero debemos usarlo así, ya que el método File.Create abre un fileStream en el archivo
                    // Y esto hace que nos salté la excepción de que no se puede usar otro stream o que el fichero está
                    //siendo usado por otro proceso
                    //File.Create(ruta).Close();

                    //Generamos el contenido
                    StreamWriter writer = new StreamWriter(ruta);
                    writer.WriteLine("---------------------------");
                    writer.WriteLine("NÓMINA DE " + e.Nombre + " " + e.Apell);
                    writer.WriteLine("Sueldo Bruto:" + e.SueldoBruto);

                    //Vemos si tiene complementos
                    if (e.Comision != null)
                        writer.WriteLine("Comisión: " + e.Comision);

                    if (e.ComplementoCargo != null)
                        writer.WriteLine("Complemento Cargo: " + e.ComplementoCargo);

                    if (e.ComplementoInvest != null)
                        writer.WriteLine("Complemento Investigacion: " + e.ComplementoInvest);

                    if (e.ComplementoResp != null)
                        writer.WriteLine("Complemento Responsabilidad: " + e.ComplementoResp);


                    writer.WriteLine("Retencion IRPF: " + e.Irpf);
                    writer.WriteLine("Retención Desempleo: " + e.Desempleo);

                    writer.WriteLine("===============================");
                    writer.WriteLine("TOTAL NETO: " + e.SueldoNeto);
                    writer.WriteLine("===============================");

                    writer.Close();
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
    }
}
