﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConexionEncriptada
{
    public partial class Form1 : Form
    {
        private SqlConnection conexion;
        private List<Empleado> empleados;

        // CONSTRUCTOR
        public Form1(SqlConnection conexion)
        {
            InitializeComponent();
            this.conexion = conexion; 
            this.empleados = new List<Empleado>();
        }


        //----------------------------------
        // BOTONES DE LOS EVENTOS
        //----------------------------------

        //---------------------
        //Nos generará los ficheros por cada empleado y el resumen para el banco
        private void btnGenerar_Click(object sender, EventArgs e)
        {
            try
            {
                // Nos rellena la lista con los datos básicos de los empleados
                generarEmpleados();
                //Nos calcula el complemento del cargo por cada empleado
                calcularComplementoCargo();
                //Nos calcula el complemento de responsabilidad/investg por cada empleado
                calcularComplementoResponsabilidad();
                calcularSueldoNeto();

                //Genera los ficheros indep con las nóminas de los empleados
                generarNominas();

                // Genera el resumen de las nóminas de todos los empleados
                generarResumenNominas();

                MessageBox.Show("Se han generado correctamente todas las nóminas", "INFO");
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "INFO");
            }            
        }
        //---------------------


        //---------------------
        //Enviará a cada empleado el fichero con su correspondiente nómina y el resumen de éste para el banco
        private void btnEnviar_Click(object sender, EventArgs e)
        {
            try
            {
                // Primero nos aseguramos que están creados los ficheros de las nóminas para poder enviarlos
                DirectoryInfo dInfo = new DirectoryInfo("ficherosNominaEmpleados");

                if (dInfo.GetFiles().Length > 0)
                {
                    // Enviamos primero el de los empleados
                    foreach (Empleado emp in empleados)
                    {
                        envioCorreo(emp.Correo, emp.RutaFicheroNomina, emp.Nombre, emp.Apell);
                    }
                    MessageBox.Show("Se han enviado correctamente todas las nóminas a sus respectivos empleados", "INFO");
                }
                else
                    MessageBox.Show("No Se han enviado las nóminas, ya que no existen", "INFO");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "INFO");
            }
        }
        //---------------------



        //----------------------------------
        // MÉTODOS AUXILIARES
        //----------------------------------

        //---------------------
        // Nos rellena el listado con los datos básicos de los empleados
        private void generarEmpleados()
        {
            SqlCommand cmd = new SqlCommand("Select * FROM empleado", this.conexion);
            SqlDataReader rd = cmd.ExecuteReader();

            while (rd.Read())
            {
                try
                {
                    Empleado e = new Empleado();
                    e.Nombre = rd["nombre"].ToString();
                    e.Apell = rd["apellido"].ToString();
                    e.IdCargo = rd["idcargo"].ToString();
                    e.IdDepart = Convert.ToInt32(rd["iddepartamento"].ToString()) / 12;
                    e.SueldoBruto = Convert.ToDouble(rd["sueldo"].ToString()) / 12;

                    if (!rd.IsDBNull(9))
                        e.Comision = Convert.ToDouble(rd["comision"].ToString()) / 12;

                    empleados.Add(e);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Algo falló a la hora de buscar a los empleados.\n" + ex.Message, "INFO");
                    rd.Close();
                }
            }
            rd.Close();
        }
        //---------------------


        //---------------------
        private void calcularComplementoCargo()
        {
            foreach (Empleado e in empleados)
            {
                SqlCommand cmd = new SqlCommand("select * from cargo where idcargo = '" + e.IdCargo + "'", this.conexion);
                SqlDataReader rd = cmd.ExecuteReader();

                while (rd.Read())
                {
                    try
                    {
                        e.ComplementoCargo = Convert.ToDouble(rd["sueldo_min"]) / 12;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Algo falló a la hora de buscar el cargo del empleado '" + e.Nombre + " " + e.Apell + "'.\n" + ex.Message, "INFO");
                        rd.Close();
                    }
                }
                rd.Close();
            }
        }
        //---------------------


        //---------------------
        private void calcularComplementoResponsabilidad()
        {
            foreach (Empleado e in empleados)
            {
                if (e.IdDepart == 100)
                    e.ComplementoResp = 16000 / 12;

                if (e.IdDepart == 102)
                    e.ComplementoResp = 10000 / 12;
            }
        }
        //---------------------


        //---------------------
        //Nos genera un fichero con el resumen de las nóminas de todos los empleados
        private void generarResumenNominas()
        {
            try
            {
                // Nos creamos por cada empleado un fichero
                string ruta = "ficherosNominaEmpleados\\Laura_RESUMEN_NOMINA.txt";
                StreamWriter writer = new StreamWriter(ruta);

                // Recorremos nuestro listado de empleados
                foreach (Empleado e in empleados)
                    writer.WriteLine("\nNÓMINA DE " + e.Nombre + " " + e.Apell + "\tSUELDO NETO: " + e.SueldoNeto);                
                writer.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Algo falló a la hora de generar las nóminas de los empleados'.\n" + ex.Message, "INFO");
            }
        }
        //---------------------


        //---------------------
        private void calcularSueldoNeto()
        {
            foreach (Empleado e in empleados)
            {

                e.Irpf = e.SueldoBruto * 0.15;
                e.Desempleo = e.SueldoBruto * 0.03;

                e.SueldoNeto = e.SueldoBruto - (e.Irpf + e.Desempleo);
            }
        }
        //---------------------


        //---------------------
        //Generamos un fichero con el nombre del empleado por cada uno
        private void generarNominas()
        {
            try
            {
                // Recorremos nuestro listado de empleados
                foreach (Empleado e in empleados)
                {
                    // Creamos un directorio para guardar las nóminas de nuestros empleados
                    if (!Directory.Exists("ficherosNominaEmpleados"))
                    {
                        Directory.CreateDirectory("ficherosNominaEmpleados");
                    }


                    // Nos creamos por cada empleado un fichero
                    string ruta = "ficherosNominaEmpleados\\Laura_Empleado" + e.Nombre + "_" + e.Apell + ".txt";
                    e.RutaFicheroNomina = ruta;

                    //Podemos usarlo o no, pero debemos usarlo así, ya que el método File.Create abre un fileStream en el archivo
                    // Y esto hace que nos salté la excepción de que no se puede usar otro stream o que el fichero está
                    //siendo usado por otro proceso
                    //File.Create(ruta).Close();

                    //Generamos el contenido
                    StreamWriter writer = new StreamWriter(ruta);
                    writer.WriteLine("---------------------------");
                    writer.WriteLine("NÓMINA DE " + e.Nombre + " " + e.Apell);
                    writer.WriteLine("Sueldo Bruto:" + e.SueldoBruto);

                    //Vemos si tiene complementos
                    if (e.Comision != null)
                        writer.WriteLine("Comisión: " + e.Comision);

                    if (e.ComplementoCargo != null)
                        writer.WriteLine("Complemento Cargo: " + e.ComplementoCargo);

                    if (e.ComplementoInvest != null)
                        writer.WriteLine("Complemento Investigacion: " + e.ComplementoInvest);

                    if (e.ComplementoResp != null)
                        writer.WriteLine("Complemento Responsabilidad: " + e.ComplementoResp);


                    writer.WriteLine("Retencion IRPF: " + e.Irpf);
                    writer.WriteLine("Retención Desempleo: " + e.Desempleo);

                    writer.WriteLine("===============================");
                    writer.WriteLine("TOTAL NETO: " + e.SueldoNeto);
                    writer.WriteLine("===============================");

                    writer.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Algo falló a la hora de generar las nóminas de los empleados'.\n" + ex.Message, "INFO");
            }
        }
        //---------------------


        //---------------------
        // Envio del correo individual
        private void envioCorreo(string correo, string rutaFicheroNomina, string nombreEmpleado, string apellidoEmpleado)
        {
            string remitente = "alumnos2damAlmunia@gmail.com";
            string miclave = "2dam2dam ";
            string destinatario = "llucbue379@iesalmunia.com";
            string asunto = "LAURA_LUCENA_" + nombreEmpleado + "_" + apellidoEmpleado;
            string mensaje = "<h2>Desde Almacenes OKISpain</h2>\n" +
                "<p>Les informamos acerca de su pedido</p>" +
                "<hr/>" +
                "<p>Aquí les dejamos todos los datos y la documentación pertinente</p>" +
                "<p>Les rogamos que sean pacientes</p>" +
                "\n<br/>" +
                "<p>Pasen un buen día y gracias por confiar en nosotros</p>" +
                "<p><b>OKISpain S.A.</b></p>";

            try
            {
                MailMessage correoMail = new MailMessage(remitente, destinatario, asunto, mensaje);
                correoMail.IsBodyHtml = true;

                //correo.Attachments.Add(new Attachment(rutaFicheroWorkcenter, MediaTypeNames.Text.Plain));
                correoMail.Attachments.Add(new Attachment(obtenerStreamFile(rutaFicheroNomina), Path.GetFileName(rutaFicheroNomina), MediaTypeNames.Text.Plain));


                SmtpClient cliente = new SmtpClient("smtp.gmail.com");
                cliente.Credentials = new NetworkCredential(remitente, miclave);
                cliente.Port = 587;
                cliente.EnableSsl = true;

                cliente.Send(correoMail);

            }
            catch (Exception ex)
            {
                MessageBox.Show("No se han podido enviar los correos a los empeados correspondientes\n" + ex.Message, "INFO");
            }
        }
        //---------------------


        //---------------------
        //Método que te devuelve el stream de un fichero, para que no se bloquee el fichero al hacer el adjuntar en el correo y poder luego borrar esos ficheros temporales
        private static Stream obtenerStreamFile(string filePath)
        {
            using (FileStream fileStream = File.OpenRead(filePath))
            {
                MemoryStream memStream = new MemoryStream();
                memStream.SetLength(fileStream.Length);
                fileStream.Read(memStream.GetBuffer(), 0, (int)fileStream.Length);

                return memStream;
            }
        }
        //---------------------

    }
}
