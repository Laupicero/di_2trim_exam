﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConexionEncriptada
{
    // Clase que nos facilitará manejas los datos de los empleados
    // La '?' nos ayudará a determinar si Double puede ser un valor nulo/null o no
    class Empleado
    {
        private string nombre;
        private string apell;
        private string correo;
        private string idCargo;
        private int idDepart;
        private Double sueldoBruto;
        private Double sueldoNeto;
        private Double? comision;
        private Double? complementoCargo;
        private Double? complementoInvest;
        private Double? complementoResp;
        private Double irpf;
        private Double desempleo;
        private string rutaFicheroNomina;

        public Empleado() { }


        //Modificadores de acceso
        public Double SueldoBruto { get => sueldoBruto; set => sueldoBruto = value; }
        public Double SueldoNeto { get => sueldoNeto; set => sueldoNeto = value; }
        public Double? Comision { get => comision; set => comision = value; }
        public Double? ComplementoCargo { get => complementoCargo; set => complementoCargo = value; }
        public Double? ComplementoInvest { get => complementoInvest; set => complementoInvest = value; }
        public Double? ComplementoResp { get => complementoResp; set => complementoResp = value; }
        public string RutaFicheroNomina { get => rutaFicheroNomina; set => rutaFicheroNomina = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Apell { get => apell; set => apell = value; }
        public string Correo { get => correo; set => correo = value; }
        public string IdCargo { get => idCargo; set => idCargo = value; }
        public int IdDepart { get => idDepart; set => idDepart = value; }
        public Double Irpf { get => irpf; set => irpf = value; }
        public Double Desempleo { get => desempleo; set => desempleo = value; }
    }
}
