﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConexionEncriptada
{
    public partial class frmLogin : Form
    {
        private SqlConnection conexion;
        private LIBRERIA.AES aes;


        // Constructor
        public frmLogin()
        {
            InitializeComponent();
            generarConexion();
            this.aes = new LIBRERIA.AES();
        }

        //----------------------------------
        // BOTONES DE LOS EVENTOS
        //----------------------------------

        //---------------
        //Botón para acceder al login
        private void btnlogin_Click(object sender, EventArgs e)
        {
            if (txtId.TextLength > 0 && txtPwd.TextLength > 0)
            {
                // Encriptamos primero la contraseña
                string pass = this.aes.Encrypt(txtPwd.Text, LIBRERIA.AES.appPwdUnique, int.Parse("256"));

                //Comprobamos las credenciales de nuestro usuario
                if(comprobarCredenciales(txtId.Text, pass))
                {
                    Form1 f1 = new Form1(this.conexion);
                    f1.ShowDialog();

                }else
                    MessageBox.Show("No se ha podido acceder a la DB\n¿Est'a seguro de qué es usted?", "INFO", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
            }
        }
        //---------------


        //---------------
        //Botón para salir de la aplicación
        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("¿Desea salir de la aplicación?", "INFO", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

            if (res == DialogResult.Yes)
                Application.Exit();
        }
        //---------------


        //----------------------------------
        // MÉTODOS AUXILIARES
        //----------------------------------


        //---------------
        // Nos genera la conexión a nuestra DB
        private void generarConexion()
        {
            try
            {
                if (File.Exists("servidor.txt"))
                {
                    StreamReader reader = File.OpenText("servidor.txt");
                    string cadenaServer = reader.ReadToEnd().Trim();
                    reader.Close();

                    string cadenaConex = "Server=" + cadenaServer + ";Database=RH; Integrated Security=true;";
                    if (cadenaServer != null)
                    {
                        conexion = new SqlConnection(cadenaConex);
                        conexion.Open();

                    }
                    else
                        MessageBox.Show("No se pudo encontrar la cadena de conexión en el fichero", "E R R O R", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                    MessageBox.Show("No se ha podido encontrar el fichero 'servidor.txt'", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Algo ha ido mal\n" + ex.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }            
        }
        //---------------



        //---------------
        // Comprobamos las credenciales de nuestro usuario, si ha ido bien nos logearemos
        private bool comprobarCredenciales(string user, string pass)
        {
            bool existe = false;
            SqlCommand cmd = new SqlCommand("Select * FROM Usuario WHERE nombreUsuario = '" + user + "'" +
                " AND contrasena='" + pass + "'", this.conexion);
            SqlDataReader rd = cmd.ExecuteReader();

            while (rd.Read())
            {
                try
                {
                    existe = true;
                }
                catch (Exception e)
                {
                    rd.Close();
                }
            }
            rd.Close();
            return existe;
        }
    }
    //---------------
}
