﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConexionEncriptada
{
    public partial class Form1 : Form
    {
        // Variable necesaria para la conexión de nuestra DB
        private SqlConnection conex;
        // Variable necesaria para obtener el listado de nuestros empleados
        private List<Empleado> empleados;

        public Form1(SqlConnection conex)
        {
            InitializeComponent();
            this.conex = conex;
            this.empleados = new List<Empleado>();
        }


        //---------------------------
        // EVENTOS BOTONES
        //---------------------------
        /// <summary>
        /// Botón que nos genera los ficheros txt de los sueldos de nuestros empleados
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGenerar_Click(object sender, EventArgs e)
        {
            //RecogidaDatos
            genEmpleados();
            calcularCargo();
            calcularDepartamento();
            calcularSueldoNeto();

            //GeneracionFicheros
            generarFicheros();
            generarResumen();

            MessageBox.Show("Se han generado correctamente los ficheros de los empelados", "INFO", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }


        /// <summary>
        /// Botón que nos enviará por correso las nóminas de nuestros empleados con su correspondiente resumen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEnviar_Click(object sender, EventArgs e)
        {
            // Primero nos aseguramos que están creados los ficheros de las nóminas para poder enviarlos
            DirectoryInfo dInfo = new DirectoryInfo("Nominas");

            if (dInfo.GetFiles().Length > 0)
            {
                enviarEmpleadosNomina();
                enviarResumenBanco();

                MessageBox.Show("Se han enviado exitosamente las nóminas y el resumen mensual de éstas", "INFO");
            }
            else
                MessageBox.Show("No Se han enviado las nóminas, ya que no existen", "INFO");
        }

        //-----------------------------------------
        //          MÉTODOS AUXILIARES
        //-----------------------------------------

        //---------------------------
        // GENERAR FICHEROS EMPLEADOS
        //---------------------------

        // Nos rellena la lista con todos los empleados y sus datos básicos
        private void genEmpleados()
        {
            string query = "Select * from empleado";
            SqlCommand cmd = new SqlCommand(query, this.conex);
            SqlDataReader reader = cmd.ExecuteReader();

            try
            {
                while (reader.Read())
                {
                    Empleado e = new Empleado();
                    e.NombreEmpleado = reader["nombre"].ToString();
                    e.ApellidoEmpleado = reader["apellido"].ToString();
                    e.IdCargo = reader["idcargo"].ToString();
                    e.IdDep = Convert.ToInt32(reader["iddepartamento"].ToString());
                    e.SueldoBase = Convert.ToDouble(reader["sueldo"].ToString()) / 12;
                    e.Email = reader["email"].ToString();

                    // Para comprobar que los campos no da null
                    if (!reader.IsDBNull(9))
                        e.Comision = Convert.ToDouble(reader["comision"].ToString()) / 12;

                    empleados.Add(e);
                }
            }
            catch (Exception ex)
            {
                reader.Close();
                MessageBox.Show("No s epuedieron obtener los datos de los empleados", "INFO", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            reader.Close();
        }

        // NOS CALCULA EL CARGO POR CADA EMPLEADO REALIZANDO UNA CONSULTA
        private void calcularCargo()
        {
            foreach (Empleado e in empleados)
            {
                string query = "select * from cargo where idcargo = '" + e.IdCargo + "'";
                SqlCommand cmd = new SqlCommand(query, this.conex);
                SqlDataReader reader = cmd.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        // Para comprobar que los campos no da null
                        if (!reader.IsDBNull(2))
                            e.CompCargo = Convert.ToDouble(reader["sueldo_min"]) / 12;
                    }
                }
                catch (Exception ex)
                {
                    reader.Close();
                    MessageBox.Show("No se pudo obtener el complemento del cargo de los empleados", "INFO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                reader.Close();
            }
        }// FIN CALCULA CARGO



        // Nos calcula si el empleado esta en el departamento de investigación o de gerencia
        private void calcularDepartamento()
        {
            foreach (Empleado e in empleados)
            {
                if (e.IdDep == 100)
                    e.CompRespon = 15500 / 6;

                if (e.IdDep == 102)
                    e.CompInvest = 6000 / 6;
            }
        }// FIN CALCULA DEPARTAMENTO



        // NOS CALCULA EL SUELDO NETO POR CADA EMPLEADO
        private void calcularSueldoNeto()
        {
            foreach (Empleado e in empleados)
            {
                e.Desempleo = e.SueldoBase * 0.04;
                e.Irpf = e.SueldoBase * 0.17;

                e.SueldoNeto = e.SueldoBase - (e.Desempleo + e.Irpf);
            }
        }// FIN CALCULA SUELDO NETO


        // Nos genera un fichero de la nómina por cada empleado
        private void generarFicheros()
        {
            try
            {
                //Nos aseguramos primero que existe el directorio 'Nominas'
                // Sino lo creamos
                if (!Directory.Exists("Nominas"))
                    Directory.CreateDirectory("Nominas");

                foreach (Empleado e in empleados)
                {
                    string fichero = "Nominas\\LAURA_LUCENA_" + e.NombreEmpleado + "_" + e.ApellidoEmpleado + ".txt";
                    e.FicheroNomina = fichero;
                    StreamWriter writer = new StreamWriter(fichero);

                    writer.WriteLine("---------------------------------");
                    writer.WriteLine("SUELDO BASE: " + e.SueldoBase);
                    writer.WriteLine("COMISIÓN: " + e.Comision);
                    writer.WriteLine("COMPLEMENTO CARGO: " + e.CompCargo);
                    writer.WriteLine("COMPLEMENTO INVESTIGACIÓN: " + e.CompInvest);
                    writer.WriteLine("COMPLEMENTO RESPONSABILIDAD: " + e.CompRespon);
                    writer.WriteLine("---------------------------------");
                    writer.WriteLine("RETENCIONES");
                    writer.WriteLine("IRPF: " + e.Irpf);
                    writer.WriteLine("DESEMPLEO: " + e.Desempleo);
                    writer.WriteLine("---------------------------------");
                    writer.WriteLine("SUELDO NETO: " + e.SueldoNeto);
                    writer.WriteLine("---------------------------------");

                    writer.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("No se han generado los ficheros/nóminas de los empleados", "INFO", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }// FIN GENERAFICHEROS()


        // Nos genera un resumen de todas las nóminas
        private void generarResumen()
        {
            if (!Directory.Exists("Nominas"))
                Directory.CreateDirectory("Nominas");

            string fichero = "Nominas\\LAURA_LUCENA_Resumen.txt";
            StreamWriter writer = new StreamWriter(fichero);

            try
            {    
                foreach (Empleado e in empleados)
                    writer.WriteLine(e.NombreEmpleado + " " + e.ApellidoEmpleado + ": Sueldo-Neto - " + e.SueldoNeto);

                writer.Close();
            }
            catch(Exception ex)
            {
                writer.Close();
                MessageBox.Show("No se ha generado el resumen de las nóminas los empleados\n" + ex.Message, "INFO", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        //---------------------------
        // ENVIAR CORREOS
        //---------------------------

        //Nos envia las nóminas de todos los empleados
        private void enviarEmpleadosNomina()
        {
            foreach(Empleado e in empleados)
            {
                string remitente = "alumnos2damAlmunia@gmail.com";
                string miclave = "2dam2dam ";
                string destinatario = e.Email;
                string asunto = "Nómina LAURA_LUCENA_" + e.NombreEmpleado + "_" + e.ApellidoEmpleado;
                string mensaje = "<h2>Desde FONTANERIA EL ESCAPE </h2>\n" +
                    "<p>Estimado" + e.NombreEmpleado + " " + e.ApellidoEmpleado + ", le envíamos su nómina en el fichero adjunto</p>";

                try
                {
                    MailMessage correoMail = new MailMessage(remitente, destinatario, asunto, mensaje);
                    //Añadimos destinatario con copia
                    MailAddress otroDestinatario = new MailAddress("alumnos2damAlmunia@gmail.com");
                    correoMail.CC.Add(otroDestinatario);

                    correoMail.IsBodyHtml = true;

                    //Esto puede generar problemas -> correo.Attachments.Add(new Attachment(e.FicheroNomina, MediaTypeNames.Text.Plain));
                    correoMail.Attachments.Add(new Attachment(obtenerStreamFile(e.FicheroNomina), Path.GetFileName(e.FicheroNomina), MediaTypeNames.Text.Plain));


                    SmtpClient cliente = new SmtpClient("smtp.gmail.com");
                    cliente.Credentials = new NetworkCredential(remitente, miclave);
                    cliente.Port = 587;
                    cliente.EnableSsl = true;

                    cliente.Send(correoMail);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("No se han podido enviar los correos a los empleados correspondientes\n" + ex.Message, "INFO");
                }
            }
        }

        //Nos envía el resumen de las nóminas de los empleados
        private void enviarResumenBanco()
        {
            string remitente = "alumnos2damAlmunia@gmail.com";
            string miclave = "2dam2dam ";
            string destinatario = "mibanco@gmalo.com";
            string asunto = "LAURA LUCENA - RESUMEN NÓMINA";
            string mensaje = "<h2>Desde FONTANERIA EL ESCAPE </h2>\n" +
                "<p>Adjuntamos resumen su nómina correspondiente al mes de Febrero</p>";

            try
            {
                MailMessage correoMail = new MailMessage(remitente, destinatario, asunto, mensaje);
                //Añadimos copia oculta
                MailAddress bcc = new MailAddress("alumnos2damAlmunia@gmail.com");
                correoMail.Bcc.Add(bcc);

                correoMail.IsBodyHtml = true;

                //correo.Attachments.Add(new Attachment("Nominas\\LAURA_LUCENA_Resumen.txt", MediaTypeNames.Text.Plain));
                correoMail.Attachments.Add(new Attachment(obtenerStreamFile("Nominas\\LAURA_LUCENA_Resumen.txt"), Path.GetFileName("Nominas\\LAURA_LUCENA_Resumen.txt"), MediaTypeNames.Text.Plain));

                SmtpClient cliente = new SmtpClient("smtp.gmail.com");
                cliente.Credentials = new NetworkCredential(remitente, miclave);
                cliente.Port = 587;
                cliente.EnableSsl = true;

                cliente.Send(correoMail);

            }
            catch (Exception ex)
            {
                MessageBox.Show("No se han podido enviar los correos a los empeados correspondientes\n" + ex.Message, "INFO");
            }
        }


        //Método que te devuelve el stream de un fichero, para que no se bloquee el fichero al hacer el adjuntar en el correo y poder luego borrar esos ficheros temporales
        private static Stream obtenerStreamFile(string filePath)
        {
            using (FileStream fileStream = File.OpenRead(filePath))
            {
                MemoryStream memStream = new MemoryStream();
                memStream.SetLength(fileStream.Length);
                fileStream.Read(memStream.GetBuffer(), 0, (int)fileStream.Length);

                return memStream;
            }
        }
    }
}
