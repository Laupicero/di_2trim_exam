﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace ConexionEncriptada
{
    

    public partial class frmLogin : Form
    {
        private SqlConnection conex;
        private LIBRERIA.AES aes;


        // Constructor
        public frmLogin()
        {
            InitializeComponent();
            this.aes = new LIBRERIA.AES();
        }


        //En el load nos generará la conexión a nuestra DB
        private void frmLogin_Load(object sender, EventArgs e)
        {
            try
            {
                generaConexion();

            }
            catch(Exception ex)
            {
                MessageBox.Show("No se pudo realiza la conexión", "INFO");
            }
            
        }


        //---------------------------
        // EVENTOS BOTONES
        //---------------------------
        // Nos realiza el login
        private void btnlogin_Click(object sender, EventArgs e)
            {
            if (txtId.TextLength > 0 && txtPwd.TextLength > 0)
            {
                string pass = this.aes.Encrypt(txtPwd.Text, LIBRERIA.AES.appPwdUnique, int.Parse("256"));

                if (comprobarCredenciales(txtId.Text, pass))
                {
                    MessageBox.Show("Usuario correcto", "INFO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Form1 f1 = new Form1(this.conex);
                    f1.ShowDialog();
                }
                else
                    MessageBox.Show("Usuario incorrecto o algo falló", "INFO");

            }
            else
                MessageBox.Show("Debe rellenar primero ambos campos", "INFO");

        }


        // Botón que nos cerrará la aplicación
        // Pero antes nos pregintará si queremos cerrarla
        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult resp = MessageBox.Show("¿Desea Salir de la aplicación?", "INFO", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

            if (resp == DialogResult.Yes)
                Application.Exit();
        }


        //---------------------------
        // MÉTODOS AUXILIARES
        //---------------------------

        //Nos genera la conexión
        private void generaConexion()
        {
            //Obtenemos el servidor del atributo del nodo del xml
            string serv = "";

            XmlReader reader = XmlReader.Create("servidor.xml");
            while (reader.Read())
            {
                if (reader.Name.ToString() == "database")
                {
                    serv = reader.GetAttribute("Miservicor");
                }
            }
            reader.Close();

            this.conex = new SqlConnection("Server = " + serv + "; Database = RH; Integrated Security = true; ");
            this.conex.Open();
        }



        


        // Nos comprueba las credenciales de nuestros usuarios
        private bool comprobarCredenciales(string nombre, string pass)
        {
            bool usuarioExiste = false;

            string query = "select * from Usuario where nombreUsuario = '" + nombre + "' and contrasena = '" + pass + "'";
            SqlCommand comand = new SqlCommand(query, this.conex);
            SqlDataReader reader = comand.ExecuteReader();
            try
            {

                while (reader.Read())
                    usuarioExiste = true;
            }
            catch
            {
                reader.Close();
                return usuarioExiste;
            }
            reader.Close();
            return usuarioExiste;
        }
    }
}
