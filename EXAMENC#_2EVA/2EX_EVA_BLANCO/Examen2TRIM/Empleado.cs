﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConexionEncriptada
{
    class Empleado
    {
        private String nombreEmpleado;
        private String apellidoEmpleado;
        private String idCargo;
        private String email;
        private String ficheroNomina;
        private int idDep;
        private Double sueldoBase;
        private Double sueldoNeto;
        private Double comision;
        private Double compCargo;
        private Double compInvest;
        private Double compRespon;
        private Double irpf;
        private Double desempleo;

        // Constructor
        public Empleado() {
            this.Comision = 0.0;
            this.CompCargo = 0.0;
            this.compInvest = 0.0;
            this.compRespon = 0.0;
        }


        //Modificadores de acceso
        public string NombreEmpleado { get => nombreEmpleado; set => nombreEmpleado = value; }
        public string ApellidoEmpleado { get => apellidoEmpleado; set => apellidoEmpleado = value; }
        public double SueldoBase { get => sueldoBase; set => sueldoBase = value; }
        public double SueldoNeto { get => sueldoNeto; set => sueldoNeto = value; }
        public double Comision { get => comision; set => comision = value; }
        public double CompCargo { get => compCargo; set => compCargo = value; }
        public double CompInvest { get => compInvest; set => compInvest = value; }
        public double CompRespon { get => compRespon; set => compRespon = value; }
        public double Irpf { get => irpf; set => irpf = value; }
        public double Desempleo { get => desempleo; set => desempleo = value; }
        public string IdCargo { get => idCargo; set => idCargo = value; }
        public int IdDep { get => idDep; set => idDep = value; }
        public string Email { get => email; set => email = value; }
        public string FicheroNomina { get => ficheroNomina; set => ficheroNomina = value; }
    }
}
